-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-01-2019 a las 01:37:41
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ospos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_app_config`
--

CREATE TABLE `ospos_app_config` (
  `key` varchar(50) NOT NULL,
  `value` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_app_config`
--

INSERT INTO `ospos_app_config` (`key`, `value`) VALUES
('address', 'Chorrillos'),
('barcode_content', 'id'),
('barcode_first_row', 'category'),
('barcode_font', 'Arial'),
('barcode_font_size', '10'),
('barcode_formats', '[]'),
('barcode_generate_if_empty', '0'),
('barcode_height', '50'),
('barcode_num_in_row', '2'),
('barcode_page_cellspacing', '20'),
('barcode_page_width', '100'),
('barcode_quality', '100'),
('barcode_second_row', 'item_code'),
('barcode_third_row', 'unit_price'),
('barcode_type', 'Code39'),
('barcode_width', '250'),
('cash_decimals', '2'),
('cash_rounding_code', '1'),
('client_id', 'f160c6f7-4236-423f-ae7c-59e067a787b7'),
('company', 'Cafetería Central'),
('company_logo', 'company_logo2.png'),
('country_codes', 'pe'),
('currency_decimals', '2'),
('currency_symbol', 'S/.'),
('custom10_name', ''),
('custom1_name', ''),
('custom2_name', ''),
('custom3_name', ''),
('custom4_name', ''),
('custom5_name', ''),
('custom6_name', ''),
('custom7_name', ''),
('custom8_name', ''),
('custom9_name', ''),
('customer_reward_enable', '0'),
('customer_sales_tax_support', '0'),
('dateformat', 'd/m/Y'),
('date_or_time_format', ''),
('default_origin_tax_code', ''),
('default_register_mode', 'sale'),
('default_sales_discount', '0'),
('default_tax_2_rate', ''),
('default_tax_category', 'Standard'),
('default_tax_rate', '8'),
('dinner_table_enable', '0'),
('email', 'rose_mary_194@hotmail.com'),
('fax', ''),
('financial_year', '1'),
('gcaptcha_enable', '0'),
('gcaptcha_secret_key', ''),
('gcaptcha_site_key', ''),
('giftcard_number', 'series'),
('invoice_default_comments', 'This is a default comment'),
('invoice_email_message', 'Dear {CU}, In attachment the receipt for sale $INV'),
('invoice_enable', '1'),
('language', 'spanish'),
('language_code', 'es'),
('last_used_invoice_number', '0'),
('last_used_quote_number', '0'),
('lines_per_page', '25'),
('line_sequence', '0'),
('mailpath', '/usr/sbin/sendmail'),
('msg_msg', ''),
('msg_pwd', ''),
('msg_src', ''),
('msg_uid', ''),
('notify_horizontal_position', 'center'),
('notify_vertical_position', 'bottom'),
('number_locale', 'PE'),
('payment_options_order', 'cashdebitcredit'),
('phone', '998800001'),
('print_bottom_margin', '0'),
('print_footer', '0'),
('print_header', '0'),
('print_left_margin', '0'),
('print_right_margin', '0'),
('print_silently', '1'),
('print_top_margin', '0'),
('protocol', 'mail'),
('quantity_decimals', '0'),
('receipt_font_size', '12'),
('receipt_show_company_name', '1'),
('receipt_show_description', '1'),
('receipt_show_serialnumber', '1'),
('receipt_show_taxes', '0'),
('receipt_show_total_discount', '1'),
('receipt_template', 'receipt_default'),
('receiving_calculate_average_price', '0'),
('recv_invoice_format', '{CO}'),
('return_policy', 'Solo se procederá a la devolución del producto si se encuentra en malas condiciones'),
('sales_invoice_format', '{CO}'),
('sales_quote_format', 'Q%y{QSEQ:6}'),
('smtp_crypto', 'ssl'),
('smtp_host', ''),
('smtp_pass', ''),
('smtp_port', '465'),
('smtp_timeout', '5'),
('smtp_user', ''),
('statistics', '1'),
('tax_decimals', '2'),
('tax_included', '0'),
('theme', 'cerulean'),
('thousands_separator', 'thousands_separator'),
('timeformat', 'H:i:s'),
('timezone', 'America/Bogota'),
('website', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_customers`
--

CREATE TABLE `ospos_customers` (
  `person_id` int(10) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `taxable` int(1) NOT NULL DEFAULT '1',
  `sales_tax_code` varchar(32) NOT NULL DEFAULT '1',
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `package_id` int(11) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_customers`
--

INSERT INTO `ospos_customers` (`person_id`, `company_name`, `account_number`, `taxable`, `sales_tax_code`, `discount_percent`, `package_id`, `points`, `deleted`) VALUES
(2, NULL, NULL, 1, '', '0.00', NULL, NULL, 0),
(3, NULL, NULL, 1, '', '0.00', NULL, NULL, 0),
(4, NULL, NULL, 1, '', '0.00', NULL, NULL, 0),
(8, NULL, NULL, 1, '', '0.00', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_customers_packages`
--

CREATE TABLE `ospos_customers_packages` (
  `package_id` int(11) NOT NULL,
  `package_name` varchar(255) DEFAULT NULL,
  `points_percent` float NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_customers_packages`
--

INSERT INTO `ospos_customers_packages` (`package_id`, `package_name`, `points_percent`, `deleted`) VALUES
(1, 'Default', 0, 0),
(2, 'Bronze', 10, 0),
(3, 'Silver', 20, 0),
(4, 'Gold', 30, 0),
(5, 'Premium', 50, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_customers_points`
--

CREATE TABLE `ospos_customers_points` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `points_earned` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_dinner_tables`
--

CREATE TABLE `ospos_dinner_tables` (
  `dinner_table_id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_dinner_tables`
--

INSERT INTO `ospos_dinner_tables` (`dinner_table_id`, `name`, `status`, `deleted`) VALUES
(1, 'Delivery', 0, 0),
(2, 'Take Away', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_employees`
--

CREATE TABLE `ospos_employees` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `hash_version` int(1) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_employees`
--

INSERT INTO `ospos_employees` (`username`, `password`, `person_id`, `deleted`, `hash_version`) VALUES
('admin', '$2y$10$wuX/BVrhSEDtMvpHqumRjuKinpTYRQGTM425AfRiephEqjj9JrzKS', 1, 0, 2),
('moderador', '$2y$10$q2fSXvhKmHZQ4iEbEGWCD.3FOI8VI7YGO/2irfVKqK4o87i9SjuW.', 7, 0, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_giftcards`
--

CREATE TABLE `ospos_giftcards` (
  `record_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `giftcard_id` int(11) NOT NULL,
  `giftcard_number` varchar(255) DEFAULT NULL,
  `value` decimal(15,2) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `person_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_giftcards`
--

INSERT INTO `ospos_giftcards` (`record_time`, `giftcard_id`, `giftcard_number`, `value`, `deleted`, `person_id`) VALUES
('2019-01-25 00:00:48', 1, '1', '0.00', 0, 4),
('2019-01-25 00:11:42', 3, '2', '2.00', 0, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_grants`
--

CREATE TABLE `ospos_grants` (
  `permission_id` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_grants`
--

INSERT INTO `ospos_grants` (`permission_id`, `person_id`) VALUES
('config', 1),
('customers', 1),
('customers', 7),
('employees', 1),
('giftcards', 1),
('items', 1),
('items_', 1),
('items_stock', 1),
('item_kits', 1),
('messages', 1),
('migrate', 1),
('receivings', 1),
('receivings_', 1),
('receivings_stock', 1),
('reports', 1),
('reports_categories', 1),
('reports_customers', 1),
('reports_discounts', 1),
('reports_employees', 1),
('reports_inventory', 1),
('reports_items', 1),
('reports_payments', 1),
('reports_receivings', 1),
('reports_sales', 1),
('reports_suppliers', 1),
('reports_taxes', 1),
('sales', 1),
('sales', 7),
('sales_', 1),
('sales_stock', 1),
('sales_stock', 7),
('suppliers', 1),
('taxes', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_inventory`
--

CREATE TABLE `ospos_inventory` (
  `trans_id` int(11) NOT NULL,
  `trans_items` int(11) NOT NULL DEFAULT '0',
  `trans_user` int(11) NOT NULL DEFAULT '0',
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text NOT NULL,
  `trans_location` int(11) NOT NULL,
  `trans_inventory` decimal(15,3) NOT NULL DEFAULT '0.000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_inventory`
--

INSERT INTO `ospos_inventory` (`trans_id`, `trans_items`, `trans_user`, `trans_date`, `trans_comment`, `trans_location`, `trans_inventory`) VALUES
(1, 1, 1, '2019-01-22 23:32:29', 'Edición Manual de Cantidad', 1, '0.000'),
(2, 1, 1, '2019-01-23 02:37:41', '#1', 1, '-1.000'),
(3, 1, 1, '2019-01-23 02:38:37', '#2', 1, '-1.000'),
(4, 1, 1, '2019-01-23 02:42:47', '#3', 1, '-1.000'),
(5, 1, 1, '2019-01-23 02:43:31', '#4', 1, '-1.000'),
(6, 1, 1, '2019-01-23 02:47:09', '#5', 1, '-5.000'),
(7, 2, 1, '2019-01-23 21:52:18', 'Edición Manual de Cantidad', 1, '20.000'),
(8, 2, 1, '2019-01-23 22:01:20', '#6', 1, '-1.000'),
(9, 2, 1, '2019-01-23 22:01:20', '#6', 1, '-1.000'),
(10, 2, 1, '2019-01-23 22:04:00', '#7', 1, '-1.000'),
(11, 2, 1, '2019-01-23 22:04:00', '#7', 1, '-1.000'),
(12, 2, 1, '2019-01-23 22:04:00', '#7', 1, '-1.000'),
(13, 2, 1, '2019-01-23 22:17:53', '#8', 1, '-1.000'),
(14, 1, 1, '2019-01-23 22:33:58', '#9', 1, '-1.000'),
(15, 2, 1, '2019-01-23 23:41:19', '#10', 1, '-1.000'),
(16, 1, 1, '2019-01-23 23:41:19', '#10', 1, '-1.000'),
(17, 1, 1, '2019-01-24 21:08:55', '#11', 1, '-5.000'),
(18, 2, 1, '2019-01-24 21:39:14', '#12', 1, '-1.000'),
(19, 1, 1, '2019-01-24 21:39:14', '#12', 1, '-1.000'),
(20, 1, 1, '2019-01-24 21:41:00', '#13', 1, '-2.000'),
(21, 2, 1, '2019-01-24 21:41:00', '#13', 1, '-1.000'),
(22, 1, 1, '2019-01-24 21:44:46', '#14', 1, '-1.000'),
(23, 1, 1, '2019-01-24 21:50:00', '#15', 1, '-1.000'),
(24, 1, 1, '2019-01-24 22:25:24', '#16', 1, '-1.000'),
(25, 1, 1, '2019-01-24 22:28:30', '#17', 1, '-1.000'),
(26, 3, 1, '2019-01-24 23:30:33', 'Edición Manual de Cantidad', 1, '0.000'),
(27, 3, 1, '2019-01-24 23:30:33', 'Edición Manual de Cantidad', 2, '0.000'),
(28, 5, 1, '2019-01-24 23:38:13', 'Edición Manual de Cantidad', 1, '0.000'),
(29, 5, 1, '2019-01-24 23:38:13', 'Edición Manual de Cantidad', 2, '0.000'),
(30, 2, 1, '2019-01-25 00:05:09', '#18', 1, '-1.000'),
(31, 2, 1, '2019-01-25 00:05:09', '#18', 1, '-1.000'),
(32, 2, 1, '2019-01-25 00:21:15', '#19', 1, '-1.000'),
(33, 1, 1, '2019-01-25 00:21:16', '#19', 1, '-1.000'),
(34, 2, 1, '2019-01-25 00:23:25', 'Edición Manual de Cantidad', 2, '10.000'),
(35, 2, 1, '2019-01-25 00:24:29', 'Eliminado', 1, '-8.000'),
(36, 3, 1, '2019-01-25 00:25:29', '#20', 1, '-5.000'),
(37, 3, 1, '2019-01-25 00:43:27', '#21', 1, '-3.000'),
(38, 5, 1, '2019-01-25 00:43:27', '#21', 1, '-1.000'),
(39, 5, 1, '2019-01-25 00:46:50', 'RECV 1', 1, '20.000'),
(40, 5, 7, '2019-01-25 00:57:38', '#22', 1, '-2.000'),
(41, 3, 7, '2019-01-25 00:57:38', '#22', 1, '-2.000'),
(42, 5, 7, '2019-01-25 01:07:39', '#23', 1, '-3.000'),
(43, 1, 1, '2019-01-25 03:53:59', '#24', 1, '-1.000'),
(44, 1, 1, '2019-01-26 02:22:52', '#25', 1, '-1.000'),
(45, 6, 1, '2019-01-26 02:44:47', 'Edición Manual de Cantidad', 1, '150.000'),
(46, 6, 1, '2019-01-26 02:44:48', 'Edición Manual de Cantidad', 2, '150.000'),
(47, 7, 1, '2019-01-26 02:46:33', 'Edición Manual de Cantidad', 1, '50.000'),
(48, 7, 1, '2019-01-26 02:46:33', 'Edición Manual de Cantidad', 2, '50.000'),
(49, 8, 1, '2019-01-26 02:50:09', 'Edición Manual de Cantidad', 1, '24.000'),
(50, 8, 1, '2019-01-26 02:50:09', 'Edición Manual de Cantidad', 2, '24.000'),
(51, 3, 1, '2019-01-26 02:56:14', '#26', 1, '-2.000'),
(52, 6, 1, '2019-01-26 02:56:14', '#26', 1, '-1.000'),
(53, 8, 1, '2019-01-26 02:56:14', '#26', 1, '-1.000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_items`
--

CREATE TABLE `ospos_items` (
  `name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `item_number` varchar(255) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `cost_price` decimal(15,2) NOT NULL,
  `unit_price` decimal(15,2) NOT NULL,
  `reorder_level` decimal(15,3) NOT NULL DEFAULT '0.000',
  `receiving_quantity` decimal(15,3) NOT NULL DEFAULT '1.000',
  `item_id` int(10) NOT NULL,
  `pic_filename` varchar(255) DEFAULT NULL,
  `allow_alt_description` tinyint(1) NOT NULL,
  `is_serialized` tinyint(1) NOT NULL,
  `stock_type` tinyint(2) NOT NULL DEFAULT '0',
  `item_type` tinyint(2) NOT NULL DEFAULT '0',
  `tax_category_id` int(10) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `custom1` varchar(255) DEFAULT NULL,
  `custom2` varchar(255) DEFAULT NULL,
  `custom3` varchar(255) DEFAULT NULL,
  `custom4` varchar(255) DEFAULT NULL,
  `custom5` varchar(255) DEFAULT NULL,
  `custom6` varchar(255) DEFAULT NULL,
  `custom7` varchar(255) DEFAULT NULL,
  `custom8` varchar(255) DEFAULT NULL,
  `custom9` varchar(255) DEFAULT NULL,
  `custom10` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_items`
--

INSERT INTO `ospos_items` (`name`, `category`, `supplier_id`, `item_number`, `description`, `cost_price`, `unit_price`, `reorder_level`, `receiving_quantity`, `item_id`, `pic_filename`, `allow_alt_description`, `is_serialized`, `stock_type`, `item_type`, `tax_category_id`, `deleted`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`, `custom6`, `custom7`, `custom8`, `custom9`, `custom10`) VALUES
('Jarabe', 'Farmacia', NULL, '7751946001127', '', '2.00', '2.50', '1.000', '1.000', 1, NULL, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', ''),
('Pan con pollo', 'Sandwich', NULL, '087219111529', '', '1.50', '3.00', '1.000', '20.000', 2, NULL, 0, 1, 0, 0, 0, 1, '', '', '', '', '', '', '', '', '', ''),
('chessseburguer', 'Sandwich', NULL, '7750084207675', '', '2.00', '3.50', '1.000', '1.000', 3, NULL, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', ''),
('Jugo de Fresa', 'Jugos', 6, '7750215136225', '', '2.50', '5.00', '1.000', '1.000', 5, NULL, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', ''),
('Menu', 'Comidas', 6, NULL, 'consta de entrada, segundo, refresco y postre', '5.50', '8.00', '1.000', '150.000', 6, NULL, 1, 1, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', ''),
('Menu sin entrada', 'comidas', 6, NULL, 'Consta de segundo, refresco y postre', '4.50', '7.00', '1.000', '50.000', 7, NULL, 1, 1, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', ''),
('Coca Cola', 'Bebidas', 9, NULL, '', '2.04', '2.80', '1.000', '24.000', 8, NULL, 0, 1, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_items_taxes`
--

CREATE TABLE `ospos_items_taxes` (
  `item_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_item_kits`
--

CREATE TABLE `ospos_item_kits` (
  `item_kit_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `item_id` int(10) NOT NULL DEFAULT '0',
  `kit_discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price_option` tinyint(2) NOT NULL DEFAULT '0',
  `print_option` tinyint(2) NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_item_kit_items`
--

CREATE TABLE `ospos_item_kit_items` (
  `item_kit_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` decimal(15,3) NOT NULL,
  `kit_sequence` int(3) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_item_quantities`
--

CREATE TABLE `ospos_item_quantities` (
  `item_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `quantity` decimal(15,3) NOT NULL DEFAULT '0.000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_item_quantities`
--

INSERT INTO `ospos_item_quantities` (`item_id`, `location_id`, `quantity`) VALUES
(1, 1, '-26.000'),
(1, 2, '0.000'),
(2, 1, '0.000'),
(2, 2, '0.000'),
(3, 1, '-12.000'),
(3, 2, '0.000'),
(5, 1, '14.000'),
(5, 2, '0.000'),
(6, 1, '149.000'),
(6, 2, '150.000'),
(7, 1, '50.000'),
(7, 2, '50.000'),
(8, 1, '23.000'),
(8, 2, '24.000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_migrations`
--

CREATE TABLE `ospos_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_migrations`
--

INSERT INTO `ospos_migrations` (`version`) VALUES
(0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_modules`
--

CREATE TABLE `ospos_modules` (
  `name_lang_key` varchar(255) NOT NULL,
  `desc_lang_key` varchar(255) NOT NULL,
  `sort` int(10) NOT NULL,
  `module_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_modules`
--

INSERT INTO `ospos_modules` (`name_lang_key`, `desc_lang_key`, `sort`, `module_id`) VALUES
('module_config', 'module_config_desc', 110, 'config'),
('module_customers', 'module_customers_desc', 10, 'customers'),
('module_employees', 'module_employees_desc', 80, 'employees'),
('module_giftcards', 'module_giftcards_desc', 90, 'giftcards'),
('module_items', 'module_items_desc', 20, 'items'),
('module_item_kits', 'module_item_kits_desc', 30, 'item_kits'),
('module_messages', 'module_messages_desc', 100, 'messages'),
('module_migrate', 'module_migrate_desc', 120, 'migrate'),
('module_receivings', 'module_receivings_desc', 60, 'receivings'),
('module_reports', 'module_reports_desc', 50, 'reports'),
('module_sales', 'module_sales_desc', 70, 'sales'),
('module_suppliers', 'module_suppliers_desc', 40, 'suppliers'),
('module_taxes', 'module_taxes_desc', 105, 'taxes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_people`
--

CREATE TABLE `ospos_people` (
  `item_number` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` int(1) DEFAULT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address_1` varchar(255) NOT NULL,
  `address_2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `person_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_people`
--

INSERT INTO `ospos_people` (`item_number`, `first_name`, `last_name`, `gender`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES
('', 'Rose Mary', 'Carrillo', NULL, '998800001', 'rose_mary_194@hotmail.com', '', '', '', '', '', '', '', 1),
('', 'Juan', 'Lopez', 1, '', '', '', '', '', '', '', '', '', 2),
('', 'Omar', 'Villa', NULL, '', '', '', '', '', '', '', '', '', 3),
('', 'Elior ', 'Espinoza', 1, '', 'elior_espinoza@hotmail.com', 'Las Torres de Matellini Edif 12 dpto. 202', '', 'Chorrillos', 'Lima', 'undefined', 'Perú', 'Tosca Mama', 4),
('', 'Wilmer', 'Arauco', 1, '976845325', '', 'Paradita chorrillos', '', 'Chorrillos', 'Lima', 'undefined', 'Perú', 'Abatecedor de pollo, huevos', 5),
('', 'Rose Mary', 'Carrillo', 0, '998800001', '', 'Av. Chorrillos s/n Villa Militar', '', 'Chorrillos', 'Lima', 'undefined', 'Perú', 'Productos hechos dentro de la Cafeteria', 6),
('', 'Isabel', 'Quincho', 0, '', '', '', '', '', '', '', '', '', 7),
('', 'Leonardo ', 'Flores', 1, '987698547', 'sandra.flores@hotmail.com', 'Las torres de matellini block 12 dpto. 202', '', 'Chorrillos', 'Lima', 'undefined', 'Perú', 'Mama Sandra Flores\nPapa Leonardo Flores\nCierres para los 20 de cada mes\nMilitar', 8),
('', 'Vega', 'Vega', NULL, '', '', '', '', '', '', '', '', '', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_permissions`
--

CREATE TABLE `ospos_permissions` (
  `permission_id` varchar(255) NOT NULL,
  `module_id` varchar(255) NOT NULL,
  `location_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_permissions`
--

INSERT INTO `ospos_permissions` (`permission_id`, `module_id`, `location_id`) VALUES
('config', 'config', NULL),
('customers', 'customers', NULL),
('employees', 'employees', NULL),
('giftcards', 'giftcards', NULL),
('items', 'items', NULL),
('items_', 'items', 2),
('items_stock', 'items', 1),
('item_kits', 'item_kits', NULL),
('messages', 'messages', NULL),
('migrate', 'migrate', NULL),
('receivings', 'receivings', NULL),
('receivings_', 'receivings', 2),
('receivings_stock', 'receivings', 1),
('reports', 'reports', NULL),
('reports_categories', 'reports', NULL),
('reports_customers', 'reports', NULL),
('reports_discounts', 'reports', NULL),
('reports_employees', 'reports', NULL),
('reports_inventory', 'reports', NULL),
('reports_items', 'reports', NULL),
('reports_payments', 'reports', NULL),
('reports_receivings', 'reports', NULL),
('reports_sales', 'reports', NULL),
('reports_suppliers', 'reports', NULL),
('reports_taxes', 'reports', NULL),
('sales', 'sales', NULL),
('sales_', 'sales', 2),
('sales_stock', 'sales', 1),
('suppliers', 'suppliers', NULL),
('taxes', 'taxes', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_receivings`
--

CREATE TABLE `ospos_receivings` (
  `receiving_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text,
  `receiving_id` int(10) NOT NULL,
  `payment_type` varchar(20) DEFAULT NULL,
  `reference` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_receivings`
--

INSERT INTO `ospos_receivings` (`receiving_time`, `supplier_id`, `employee_id`, `comment`, `receiving_id`, `payment_type`, `reference`) VALUES
('2019-01-25 00:46:50', 6, 1, '', 1, 'Efectivo', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_receivings_items`
--

CREATE TABLE `ospos_receivings_items` (
  `receiving_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL,
  `receiving_quantity` decimal(15,3) NOT NULL DEFAULT '1.000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_receivings_items`
--

INSERT INTO `ospos_receivings_items` (`receiving_id`, `item_id`, `description`, `serialnumber`, `line`, `quantity_purchased`, `item_cost_price`, `item_unit_price`, `discount_percent`, `item_location`, `receiving_quantity`) VALUES
(1, 5, '', NULL, 1, '20.000', '2.50', '2.50', '0.00', 1, '1.000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_sales`
--

CREATE TABLE `ospos_sales` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text,
  `invoice_number` varchar(32) DEFAULT NULL,
  `quote_number` varchar(32) DEFAULT NULL,
  `sale_id` int(10) NOT NULL,
  `sale_status` tinyint(2) NOT NULL DEFAULT '0',
  `dinner_table_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_sales`
--

INSERT INTO `ospos_sales` (`sale_time`, `customer_id`, `employee_id`, `comment`, `invoice_number`, `quote_number`, `sale_id`, `sale_status`, `dinner_table_id`) VALUES
('2019-01-23 02:37:40', 3, 1, '', NULL, NULL, 1, 0, NULL),
('2019-01-23 02:38:37', NULL, 1, '', NULL, NULL, 2, 0, NULL),
('2019-01-23 02:42:47', NULL, 1, '', '0', NULL, 3, 0, NULL),
('2019-01-23 02:43:31', NULL, 1, '', NULL, NULL, 4, 0, NULL),
('2019-01-23 02:47:09', NULL, 1, '', NULL, NULL, 5, 0, NULL),
('2019-01-23 22:01:20', 4, 1, '', NULL, NULL, 6, 0, NULL),
('2019-01-23 22:04:00', 4, 1, '', NULL, NULL, 7, 0, NULL),
('2019-01-23 22:17:53', NULL, 1, '', NULL, NULL, 8, 0, NULL),
('2019-01-23 22:33:58', NULL, 1, '', NULL, NULL, 9, 0, NULL),
('2019-01-23 23:41:19', NULL, 1, '', NULL, NULL, 10, 0, NULL),
('2019-01-24 21:08:55', NULL, 1, '', NULL, NULL, 11, 0, NULL),
('2019-01-24 21:39:14', 4, 1, '', NULL, NULL, 12, 0, NULL),
('2019-01-24 21:41:00', 3, 1, '', NULL, NULL, 13, 0, NULL),
('2019-01-24 21:44:46', NULL, 1, '', NULL, NULL, 14, 0, NULL),
('2019-01-24 21:50:00', NULL, 1, '', NULL, NULL, 15, 0, NULL),
('2019-01-24 22:25:24', NULL, 1, '', NULL, NULL, 16, 0, NULL),
('2019-01-24 22:28:30', NULL, 1, '', NULL, NULL, 17, 0, NULL),
('2019-01-25 00:05:09', 4, 1, '', NULL, NULL, 18, 0, NULL),
('2019-01-25 00:21:15', NULL, 1, '', NULL, NULL, 19, 0, NULL),
('2019-01-25 00:25:29', NULL, 1, '', NULL, NULL, 20, 0, NULL),
('2019-01-25 00:43:26', 2, 1, '', NULL, NULL, 21, 0, NULL),
('2019-01-25 00:57:38', 2, 7, '', NULL, NULL, 22, 0, NULL),
('2019-01-25 01:07:39', 2, 7, 'pensionado', NULL, NULL, 23, 0, NULL),
('2019-01-25 03:53:59', NULL, 1, '', NULL, NULL, 24, 0, NULL),
('2019-01-26 02:22:52', 2, 1, '', NULL, NULL, 25, 0, NULL),
('2019-01-26 02:56:14', 8, 1, 'pensionado', NULL, NULL, 26, 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_sales_items`
--

CREATE TABLE `ospos_sales_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL,
  `print_option` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_sales_items`
--

INSERT INTO `ospos_sales_items` (`sale_id`, `item_id`, `description`, `serialnumber`, `line`, `quantity_purchased`, `item_cost_price`, `item_unit_price`, `discount_percent`, `item_location`, `print_option`) VALUES
(1, 1, '', '', 1, '1.000', '2.00', '2.50', '0.00', 1, 0),
(2, 1, '', '', 1, '1.000', '2.00', '2.50', '0.00', 1, 0),
(3, 1, '', '', 1, '1.000', '2.00', '2.50', '0.00', 1, 0),
(4, 1, '', '', 1, '1.000', '2.00', '2.50', '0.00', 1, 0),
(5, 1, '', '', 1, '5.000', '2.00', '2.50', '0.00', 1, 0),
(6, 2, '', '', 1, '1.000', '1.50', '3.00', '0.00', 1, 0),
(6, 2, '', '', 2, '1.000', '1.50', '3.00', '0.00', 1, 0),
(7, 2, '', '', 1, '1.000', '1.50', '3.00', '0.00', 1, 0),
(7, 2, '', '', 2, '1.000', '1.50', '3.00', '0.00', 1, 0),
(7, 2, '', '', 3, '1.000', '1.50', '3.00', '0.00', 1, 0),
(8, 2, '', '', 1, '1.000', '1.50', '3.00', '0.00', 1, 0),
(9, 1, '', '', 1, '1.000', '2.00', '2.50', '0.00', 1, 0),
(10, 1, '', '', 2, '1.000', '2.00', '2.50', '0.00', 1, 0),
(10, 2, '', '', 1, '1.000', '1.50', '3.00', '0.00', 1, 0),
(11, 1, '', '', 1, '5.000', '2.00', '2.50', '0.00', 1, 0),
(12, 1, '', '', 2, '1.000', '2.00', '2.50', '0.00', 1, 0),
(12, 2, '', '', 1, '1.000', '1.50', '3.00', '0.00', 1, 0),
(13, 1, '', '', 1, '2.000', '2.00', '2.50', '0.00', 1, 0),
(13, 2, '', '', 2, '1.000', '1.50', '3.00', '0.00', 1, 0),
(14, 1, '', '', 1, '1.000', '2.00', '2.50', '0.00', 1, 0),
(15, 1, '', '', 1, '1.000', '2.00', '2.50', '0.00', 1, 0),
(16, 1, '', '', 1, '1.000', '2.00', '2.50', '0.00', 1, 0),
(17, 1, '', '', 1, '1.000', '2.00', '2.50', '0.00', 1, 0),
(18, 2, '', '', 1, '1.000', '1.50', '3.00', '0.00', 1, 0),
(18, 2, '', '', 2, '1.000', '1.50', '3.00', '0.00', 1, 0),
(19, 1, '', '', 2, '1.000', '2.00', '2.50', '0.00', 1, 0),
(19, 2, '', '', 1, '1.000', '1.50', '3.00', '0.00', 1, 0),
(20, 3, '', '', 1, '5.000', '2.00', '3.50', '0.00', 1, 0),
(21, 3, '', '', 1, '3.000', '2.00', '3.50', '0.00', 1, 0),
(21, 5, '', '', 2, '1.000', '2.50', '5.00', '0.00', 1, 0),
(22, 3, '', '', 2, '2.000', '2.00', '3.50', '0.00', 1, 0),
(22, 5, '', '', 1, '2.000', '2.50', '5.00', '0.00', 1, 0),
(23, 5, '', '', 1, '3.000', '2.50', '5.00', '0.00', 1, 0),
(24, 1, '', '', 1, '1.000', '2.00', '2.50', '0.00', 1, 0),
(25, 1, '', '', 1, '1.000', '2.00', '2.50', '0.00', 1, 0),
(26, 3, '', '', 1, '2.000', '2.00', '3.50', '0.00', 1, 0),
(26, 6, 'consta de entrada, segundo, refresco y postre', '', 2, '1.000', '5.50', '8.00', '0.00', 1, 0),
(26, 8, '', '', 3, '1.000', '2.04', '2.80', '0.00', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_sales_items_taxes`
--

CREATE TABLE `ospos_sales_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax_type` tinyint(2) NOT NULL DEFAULT '0',
  `rounding_code` tinyint(2) NOT NULL DEFAULT '0',
  `cascade_tax` tinyint(2) NOT NULL DEFAULT '0',
  `cascade_sequence` tinyint(2) NOT NULL DEFAULT '0',
  `item_tax_amount` decimal(15,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_sales_payments`
--

CREATE TABLE `ospos_sales_payments` (
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_sales_payments`
--

INSERT INTO `ospos_sales_payments` (`sale_id`, `payment_type`, `payment_amount`) VALUES
(1, 'Efectivo', '10.00'),
(2, 'Tarjeta de Débito', '2.50'),
(3, 'Efectivo', '2.50'),
(4, 'Efectivo', '2.50'),
(5, 'Cheque', '12.50'),
(6, 'Efectivo', '6.00'),
(7, 'sales_due (TBD)', '9.00'),
(8, 'Efectivo', '10.00'),
(9, 'Efectivo', '2.50'),
(10, 'Cheque', '5.50'),
(11, 'Efectivo', '12.50'),
(12, 'Efectivo', '5.50'),
(13, 'Efectivo', '20.00'),
(14, 'Efectivo', '2.50'),
(15, 'Efectivo', '2.50'),
(16, 'Efectivo', '2.50'),
(17, 'Efectivo', '2.50'),
(18, 'Efectivo', '3.00'),
(18, 'Tarjeta de Regalo:1', '3.00'),
(19, 'Efectivo', '50.00'),
(20, 'Cheque', '17.50'),
(21, 'Cheque', '15.50'),
(22, 'Efectivo', '20.00'),
(23, 'Cheque', '15.00'),
(24, 'Efectivo', '2.50'),
(25, 'Efectivo', '20.00'),
(26, 'Cheque', '17.80');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_sales_reward_points`
--

CREATE TABLE `ospos_sales_reward_points` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `earned` float NOT NULL,
  `used` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_sales_taxes`
--

CREATE TABLE `ospos_sales_taxes` (
  `sale_id` int(10) NOT NULL,
  `tax_type` smallint(2) NOT NULL,
  `tax_group` varchar(32) NOT NULL,
  `sale_tax_basis` decimal(15,4) NOT NULL,
  `sale_tax_amount` decimal(15,4) NOT NULL,
  `print_sequence` tinyint(2) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `tax_rate` decimal(15,4) NOT NULL,
  `sales_tax_code` varchar(32) NOT NULL DEFAULT '',
  `rounding_code` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_sessions`
--

CREATE TABLE `ospos_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_sessions`
--

INSERT INTO `ospos_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('j7tuo92rbc7utc1o9r8vii8mcjbnrkrt', '::1', 1548365936, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383336353632353b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c733a353a2266616c7365223b73616c65735f636172747c613a323a7b693a313b613a31393a7b733a373a226974656d5f6964223b733a313a2232223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a31323a22696e76656e746172696f2031223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a31333a2250616e20636f6e20706f6c6c6f223b733a31313a226974656d5f6e756d626572223b733a31323a22303837323139313131353239223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2231223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a343a22302e3030223b733a383a22696e5f73746f636b223b733a363a2231332e303030223b733a353a227072696365223b733a343a22332e3030223b733a353a22746f74616c223b733a343a22332e3030223b733a31363a22646973636f756e7465645f746f74616c223b733a363a22332e30303030223b733a31323a227072696e745f6f7074696f6e223b733a313a2230223b733a31303a2273746f636b5f74797065223b693a303b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d693a323b613a31393a7b733a373a226974656d5f6964223b733a313a2231223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a31323a22696e76656e746172696f2031223b733a343a226c696e65223b693a323b733a343a226e616d65223b733a363a224a6172616265223b733a31313a226974656d5f6e756d626572223b733a31333a2237373531393436303031313237223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a373a222d31362e303030223b733a353a227072696365223b733a343a22322e3530223b733a353a22746f74616c223b733a343a22322e3530223b733a31363a22646973636f756e7465645f746f74616c223b733a363a22322e35303030223b733a31323a227072696e745f6f7074696f6e223b733a313a2230223b733a31303a2273746f636b5f74797065223b693a303b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f637573746f6d65727c733a313a2234223b73616c65735f7061796d656e74737c613a313a7b733a383a22456665637469766f223b613a323a7b733a31323a227061796d656e745f74797065223b733a383a22456665637469766f223b733a31343a227061796d656e745f616d6f756e74223b643a352e353b7d7d636173685f6d6f64657c693a313b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b73616c65735f656d61696c5f726563656970747c733a313a2231223b),
('e047vq0g0529v8e8ectmqbubaalq0efm', '::1', 1548366278, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383336353935343b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f656d706c6f7965657c733a313a2231223b73616c65735f636172747c613a303a7b7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a313b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b),
('am89a57mquksgh38460gbdq2u4qf5gc7', '::1', 1548366287, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383336363238303b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f656d706c6f7965657c733a313a2231223b),
('2i3ubkbacpcbfd0hhtrkm778puf8divf', '::1', 1548366600, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383336363538363b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f656d706c6f7965657c733a313a2231223b),
('he3qq0vi90mf69ah51vo6hmc5v4si0ue', '::1', 1548368725, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383336383731323b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f656d706c6f7965657c733a313a2231223b),
('qvup5vdppvqjlbjicbosds0uppm2f45s', '::1', 1548369038, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383336383838313b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b),
('sbiaec9if9s8lmfmdno0sore0sn057kk', '::1', 1548368965, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383336383936353b73657373696f6e5f736861317c733a373a2234663561643537223b),
('jeh0k88ft3teoiq4ao40h2m7f53d807q', '::1', 1548370477, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383337303436343b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f656d706c6f7965657c733a313a2231223b),
('62el8ke7ja42h6lpn6rk6d4shgsd6irf', '::1', 1548372751, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383337323435303b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f656d706c6f7965657c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('7jg3a6puqj5snf9dq0ifc0qokpcmq4uq', '::1', 1548373093, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383337323737363b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f656d706c6f7965657c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b726563765f636172747c613a303a7b7d726563765f6d6f64657c733a373a2272656365697665223b726563765f73746f636b5f736f757263657c733a313a2231223b726563765f73746f636b5f64657374696e6174696f6e7c733a313a2231223b726563765f737570706c6965727c733a313a2236223b),
('nar6h83d627tlggjt29glnke2kjro940', '::1', 1548373419, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383337333039343b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f656d706c6f7965657c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b726563765f73746f636b5f736f757263657c733a313a2231223b726563765f73746f636b5f64657374696e6174696f6e7c733a313a2231223b726563765f636172747c613a303a7b7d726563765f6d6f64657c733a373a2272656365697665223b726563765f737570706c6965727c693a2d313b),
('0n60nc517o8j3mrlse7vs81i4us5ft06', '::1', 1548373431, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383337333432323b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f656d706c6f7965657c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b726563765f73746f636b5f736f757263657c733a313a2231223b726563765f73746f636b5f64657374696e6174696f6e7c733a313a2231223b726563765f636172747c613a303a7b7d726563765f6d6f64657c733a373a2272656365697665223b726563765f737570706c6965727c693a2d313b),
('2iq1g9937bs68njuktpfs2k03cm6o8tm', '::1', 1548374605, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383337343331313b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f656d706c6f7965657c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b726563765f73746f636b5f736f757263657c733a313a2231223b726563765f73746f636b5f64657374696e6174696f6e7c733a313a2231223b726563765f636172747c613a303a7b7d726563765f6d6f64657c733a373a2272656365697665223b726563765f737570706c6965727c693a2d313b73616c65735f636172747c613a323a7b693a313b613a31393a7b733a373a226974656d5f6964223b733a313a2232223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a31323a22696e76656e746172696f2031223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a31333a2250616e20636f6e20706f6c6c6f223b733a31313a226974656d5f6e756d626572223b733a31323a22303837323139313131353239223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2231223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a363a2231312e303030223b733a353a227072696365223b733a343a22332e3030223b733a353a22746f74616c223b733a343a22332e3030223b733a31363a22646973636f756e7465645f746f74616c223b733a363a22332e30303030223b733a31323a227072696e745f6f7074696f6e223b733a313a2230223b733a31303a2273746f636b5f74797065223b693a303b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d693a323b613a31393a7b733a373a226974656d5f6964223b733a313a2232223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a31323a22696e76656e746172696f2031223b733a343a226c696e65223b693a323b733a343a226e616d65223b733a31333a2250616e20636f6e20706f6c6c6f223b733a31313a226974656d5f6e756d626572223b733a31323a22303837323139313131353239223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2231223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a363a2231312e303030223b733a353a227072696365223b733a343a22332e3030223b733a353a22746f74616c223b733a343a22332e3030223b733a31363a22646973636f756e7465645f746f74616c223b733a363a22332e30303030223b733a31323a227072696e745f6f7074696f6e223b733a313a2230223b733a31303a2273746f636b5f74797065223b693a303b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f637573746f6d65727c733a313a2234223b73616c65735f7061796d656e74737c613a313a7b733a31393a225461726a65746120646520526567616c6f3a31223b613a323a7b733a31323a227061796d656e745f74797065223b733a31393a225461726a65746120646520526567616c6f3a31223b733a31343a227061796d656e745f616d6f756e74223b733a343a22332e3030223b7d7d636173685f6d6f64657c693a313b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b73616c65735f67696674636172645f72656d61696e6465727c693a303b),
('ca0lpd058laj6avojdeqkij1nqdg36ji', '::1', 1548374710, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383337343639383b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f656d706c6f7965657c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b726563765f73746f636b5f736f757263657c733a313a2231223b726563765f73746f636b5f64657374696e6174696f6e7c733a313a2231223b726563765f636172747c613a303a7b7d726563765f6d6f64657c733a373a2272656365697665223b726563765f737570706c6965727c693a2d313b),
('umhms9n2pknlpe0sptrkdj0e4nasgdkd', '::1', 1548375142, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383337343834373b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f656d706c6f7965657c733a313a2231223b),
('so5jenqs9p6dnl6asit6gu9f7ovmqnmt', '::1', 1548375518, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383337353231303b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f656d706c6f7965657c733a313a2231223b726563765f636172747c613a303a7b7d726563765f6d6f64657c733a373a2272656365697665223b726563765f73746f636b5f736f757263657c733a313a2231223b726563765f73746f636b5f64657374696e6174696f6e7c733a313a2231223b726563765f737570706c6965727c693a2d313b73616c65735f636172747c613a313a7b693a313b613a31393a7b733a373a226974656d5f6964223b733a313a2232223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a31323a22696e76656e746172696f2031223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a31333a2250616e20636f6e20706f6c6c6f223b733a31313a226974656d5f6e756d626572223b733a31323a22303837323139313131353239223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2231223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a353a22392e303030223b733a353a227072696365223b733a343a22332e3030223b733a353a22746f74616c223b733a343a22332e3030223b733a31363a22646973636f756e7465645f746f74616c223b733a363a22332e30303030223b733a31323a227072696e745f6f7074696f6e223b733a313a2230223b733a31303a2273746f636b5f74797065223b693a303b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a313b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b),
('188krqr7ni81l3qkedcgfliprjfj0mcf', '::1', 1548375847, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383337353532313b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f656d706c6f7965657c733a313a2231223b726563765f636172747c613a303a7b7d726563765f6d6f64657c733a373a2272656365697665223b726563765f73746f636b5f736f757263657c733a313a2231223b726563765f73746f636b5f64657374696e6174696f6e7c733a313a2231223b726563765f737570706c6965727c693a2d313b73616c65735f7072696e745f61667465725f73616c657c733a343a2274727565223b73616c65735f636172747c613a303a7b7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a313b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('f5nko4phg43hd6tsbj4re99prk0rl0bh', '::1', 1548375957, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383337353834393b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f656d706c6f7965657c733a313a2231223b726563765f636172747c613a303a7b7d726563765f6d6f64657c733a373a2272656365697665223b726563765f73746f636b5f736f757263657c733a313a2231223b726563765f73746f636b5f64657374696e6174696f6e7c733a313a2231223b726563765f737570706c6965727c693a2d313b73616c65735f7072696e745f61667465725f73616c657c733a343a2274727565223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('fbarndo9mt14o7cp9q34lejq78av3s6p', '::1', 1548377090, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383337363836303b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a303a7b7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a313b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b726563765f636172747c613a313a7b693a313b613a31353a7b733a373a226974656d5f6964223b733a313a2235223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a31323a22696e76656e746172696f2031223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a31333a224a75676f206465204672657361223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b4e3b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b643a32303b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a363a222d312e303030223b733a353a227072696365223b643a322e353b733a31383a22726563656976696e675f7175616e74697479223b733a353a22312e303030223b733a353a22746f74616c223b733a373a2235302e30303030223b7d7d726563765f6d6f64657c733a373a2272656365697665223b726563765f73746f636b5f736f757263657c733a313a2231223b726563765f73746f636b5f64657374696e6174696f6e7c733a313a2231223b726563765f737570706c6965727c733a313a2236223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('knntdstph5qo2ciispjdsao89q4d13ms', '::1', 1548377677, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383337373231303b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a303a7b7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a313b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b726563765f73746f636b5f736f757263657c733a313a2231223b726563765f73746f636b5f64657374696e6174696f6e7c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('m3a6saf2v80pcf5o964hhn0706q315gs', '::1', 1548377859, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383337373638373b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2237223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b),
('ph0u5stlb0lfkm1ab59404rjvub9056l', '::1', 1548378600, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383337383630303b73657373696f6e5f736861317c733a373a2234663561643537223b),
('tc9h1l5g7simasibpsh0k6v4hahc060q', '::1', 1548388439, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383338383337333b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f7072696e745f61667465725f73616c657c733a343a2274727565223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b),
('aclfome76ddhgp39h81gu5jkc4tpsdmv', '::1', 1548469295, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383436393239333b73657373696f6e5f736861317c733a373a2234663561643537223b),
('a97gdd7006c72ckkfni31llvtr1qv3et', '::1', 1548469811, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383436393239333b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a303a7b7d73616c65735f637573746f6d65727c733a313a2234223b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a313b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b),
('s5g2qutdl14ohfn4hvdtmqqmkk58ef89', '::1', 1548469825, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383436393832343b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a303a7b7d73616c65735f637573746f6d65727c733a313a2234223b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a313b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b),
('7b9lof6kh1la2ng2d5pjlo8sh2et1n5p', '::1', 1548470506, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383437303138343b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a313a7b693a313b613a31393a7b733a373a226974656d5f6964223b733a313a2233223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a31323a22696e76656e746172696f2031223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a31343a226368657373736562757267756572223b733a31313a226974656d5f6e756d626572223b733a31333a2237373530303834323037363735223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b643a323b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a373a222d31302e303030223b733a353a227072696365223b643a332e353b733a353a22746f74616c223b733a333a22372e30223b733a31363a22646973636f756e7465645f746f74616c223b733a363a22372e30303030223b733a31323a227072696e745f6f7074696f6e223b733a313a2230223b733a31303a2273746f636b5f74797065223b693a303b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a313b636173685f726f756e64696e677c693a303b73616c65735f637573746f6d65727c733a313a2238223b73616c65735f696e766f6963655f6e756d6265727c4e3b),
('m5rsqtff4v348652l776kn7d196fhm3b', '::1', 1548470862, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383437303531313b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a313a7b693a313b613a31393a7b733a373a226974656d5f6964223b733a313a2233223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a31323a22696e76656e746172696f2031223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a31343a226368657373736562757267756572223b733a31313a226974656d5f6e756d626572223b733a31333a2237373530303834323037363735223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b643a323b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a373a222d31302e303030223b733a353a227072696365223b643a332e353b733a353a22746f74616c223b733a333a22372e30223b733a31363a22646973636f756e7465645f746f74616c223b733a363a22372e30303030223b733a31323a227072696e745f6f7074696f6e223b733a313a2230223b733a31303a2273746f636b5f74797065223b693a303b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a313b636173685f726f756e64696e677c693a303b73616c65735f637573746f6d65727c733a313a2238223b73616c65735f696e766f6963655f6e756d6265727c4e3b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('8rk4ieq4e43i4e4vreinnitvokcbgshk', '::1', 1548471172, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383437303837373b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a313a7b693a313b613a31393a7b733a373a226974656d5f6964223b733a313a2233223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a31323a22696e76656e746172696f2031223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a31343a226368657373736562757267756572223b733a31313a226974656d5f6e756d626572223b733a31333a2237373530303834323037363735223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b643a323b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a373a222d31302e303030223b733a353a227072696365223b643a332e353b733a353a22746f74616c223b733a333a22372e30223b733a31363a22646973636f756e7465645f746f74616c223b733a363a22372e30303030223b733a31323a227072696e745f6f7074696f6e223b733a313a2230223b733a31303a2273746f636b5f74797065223b693a303b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a313b636173685f726f756e64696e677c693a303b73616c65735f637573746f6d65727c733a313a2238223b73616c65735f696e766f6963655f6e756d6265727c4e3b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('cb24bme0vcokus99rs33g08r2trsfqvq', '::1', 1548471513, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383437313230313b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b6974656d5f6c6f636174696f6e7c733a313a2231223b73616c65735f656d706c6f7965657c733a313a2231223b),
('1j4kki22idg3q3dr3kq783t752qkv9hp', '::1', 1548471757, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383437313532333b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b6974656d5f6c6f636174696f6e7c733a313a2231223b73616c65735f656d706c6f7965657c733a313a2231223b726563765f636172747c613a303a7b7d726563765f6d6f64657c733a373a2272656365697665223b726563765f73746f636b5f736f757263657c733a313a2231223b726563765f73746f636b5f64657374696e6174696f6e7c733a313a2231223b726563765f737570706c6965727c693a2d313b),
('j2kbhcn6i087o043b6pkn3m8efuso3e6', '::1', 1548471969, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383437313936393b73657373696f6e5f736861317c733a373a2234663561643537223b),
('ank48ai2e4btuo2jr05uqavuce10v4v4', '::1', 1548635756, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534383633353637333b73657373696f6e5f736861317c733a373a2234663561643537223b706572736f6e5f69647c733a313a2231223b);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_stock_locations`
--

CREATE TABLE `ospos_stock_locations` (
  `location_id` int(11) NOT NULL,
  `location_name` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_stock_locations`
--

INSERT INTO `ospos_stock_locations` (`location_id`, `location_name`, `deleted`) VALUES
(1, 'inventario 1', 0),
(2, '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_suppliers`
--

CREATE TABLE `ospos_suppliers` (
  `person_id` int(10) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `agency_name` varchar(255) NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_suppliers`
--

INSERT INTO `ospos_suppliers` (`person_id`, `company_name`, `agency_name`, `account_number`, `deleted`) VALUES
(5, 'Pollos', '', NULL, 0),
(6, 'Cafeteria', '', NULL, 0),
(9, 'Corporacion Vega', '', NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_tax_categories`
--

CREATE TABLE `ospos_tax_categories` (
  `tax_category_id` int(10) NOT NULL,
  `tax_category` varchar(32) NOT NULL,
  `tax_group_sequence` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ospos_tax_categories`
--

INSERT INTO `ospos_tax_categories` (`tax_category_id`, `tax_category`, `tax_group_sequence`) VALUES
(1, 'Standard', 10),
(2, 'Service', 12),
(3, 'Alcohol', 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_tax_codes`
--

CREATE TABLE `ospos_tax_codes` (
  `tax_code` varchar(32) NOT NULL,
  `tax_code_name` varchar(255) NOT NULL DEFAULT '',
  `tax_code_type` tinyint(2) NOT NULL DEFAULT '0',
  `city` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ospos_tax_code_rates`
--

CREATE TABLE `ospos_tax_code_rates` (
  `rate_tax_code` varchar(32) NOT NULL,
  `rate_tax_category_id` int(10) NOT NULL,
  `tax_rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `rounding_code` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ospos_app_config`
--
ALTER TABLE `ospos_app_config`
  ADD PRIMARY KEY (`key`);

--
-- Indices de la tabla `ospos_customers`
--
ALTER TABLE `ospos_customers`
  ADD UNIQUE KEY `account_number` (`account_number`),
  ADD KEY `person_id` (`person_id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indices de la tabla `ospos_customers_packages`
--
ALTER TABLE `ospos_customers_packages`
  ADD PRIMARY KEY (`package_id`);

--
-- Indices de la tabla `ospos_customers_points`
--
ALTER TABLE `ospos_customers_points`
  ADD PRIMARY KEY (`id`),
  ADD KEY `person_id` (`person_id`),
  ADD KEY `package_id` (`package_id`),
  ADD KEY `sale_id` (`sale_id`);

--
-- Indices de la tabla `ospos_dinner_tables`
--
ALTER TABLE `ospos_dinner_tables`
  ADD PRIMARY KEY (`dinner_table_id`);

--
-- Indices de la tabla `ospos_employees`
--
ALTER TABLE `ospos_employees`
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `person_id` (`person_id`);

--
-- Indices de la tabla `ospos_giftcards`
--
ALTER TABLE `ospos_giftcards`
  ADD PRIMARY KEY (`giftcard_id`),
  ADD UNIQUE KEY `giftcard_number` (`giftcard_number`),
  ADD KEY `person_id` (`person_id`);

--
-- Indices de la tabla `ospos_grants`
--
ALTER TABLE `ospos_grants`
  ADD PRIMARY KEY (`permission_id`,`person_id`);

--
-- Indices de la tabla `ospos_inventory`
--
ALTER TABLE `ospos_inventory`
  ADD PRIMARY KEY (`trans_id`),
  ADD KEY `trans_items` (`trans_items`),
  ADD KEY `trans_user` (`trans_user`),
  ADD KEY `trans_location` (`trans_location`);

--
-- Indices de la tabla `ospos_items`
--
ALTER TABLE `ospos_items`
  ADD PRIMARY KEY (`item_id`),
  ADD UNIQUE KEY `item_number` (`item_number`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indices de la tabla `ospos_items_taxes`
--
ALTER TABLE `ospos_items_taxes`
  ADD PRIMARY KEY (`item_id`,`name`,`percent`);

--
-- Indices de la tabla `ospos_item_kits`
--
ALTER TABLE `ospos_item_kits`
  ADD PRIMARY KEY (`item_kit_id`);

--
-- Indices de la tabla `ospos_item_kit_items`
--
ALTER TABLE `ospos_item_kit_items`
  ADD PRIMARY KEY (`item_kit_id`,`item_id`,`quantity`),
  ADD KEY `ospos_item_kit_items_ibfk_2` (`item_id`);

--
-- Indices de la tabla `ospos_item_quantities`
--
ALTER TABLE `ospos_item_quantities`
  ADD PRIMARY KEY (`item_id`,`location_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `location_id` (`location_id`);

--
-- Indices de la tabla `ospos_modules`
--
ALTER TABLE `ospos_modules`
  ADD PRIMARY KEY (`module_id`),
  ADD UNIQUE KEY `desc_lang_key` (`desc_lang_key`),
  ADD UNIQUE KEY `name_lang_key` (`name_lang_key`);

--
-- Indices de la tabla `ospos_people`
--
ALTER TABLE `ospos_people`
  ADD PRIMARY KEY (`person_id`),
  ADD KEY `email` (`email`);

--
-- Indices de la tabla `ospos_permissions`
--
ALTER TABLE `ospos_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `module_id` (`module_id`);

--
-- Indices de la tabla `ospos_receivings`
--
ALTER TABLE `ospos_receivings`
  ADD PRIMARY KEY (`receiving_id`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `reference` (`reference`);

--
-- Indices de la tabla `ospos_receivings_items`
--
ALTER TABLE `ospos_receivings_items`
  ADD PRIMARY KEY (`receiving_id`,`item_id`,`line`),
  ADD KEY `item_id` (`item_id`);

--
-- Indices de la tabla `ospos_sales`
--
ALTER TABLE `ospos_sales`
  ADD PRIMARY KEY (`sale_id`),
  ADD UNIQUE KEY `invoice_number` (`invoice_number`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `sale_time` (`sale_time`),
  ADD KEY `dinner_table_id` (`dinner_table_id`);

--
-- Indices de la tabla `ospos_sales_items`
--
ALTER TABLE `ospos_sales_items`
  ADD PRIMARY KEY (`sale_id`,`item_id`,`line`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `item_location` (`item_location`);

--
-- Indices de la tabla `ospos_sales_items_taxes`
--
ALTER TABLE `ospos_sales_items_taxes`
  ADD PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indices de la tabla `ospos_sales_payments`
--
ALTER TABLE `ospos_sales_payments`
  ADD PRIMARY KEY (`sale_id`,`payment_type`),
  ADD KEY `sale_id` (`sale_id`);

--
-- Indices de la tabla `ospos_sales_reward_points`
--
ALTER TABLE `ospos_sales_reward_points`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sale_id` (`sale_id`);

--
-- Indices de la tabla `ospos_sales_taxes`
--
ALTER TABLE `ospos_sales_taxes`
  ADD PRIMARY KEY (`sale_id`,`tax_type`,`tax_group`),
  ADD KEY `print_sequence` (`sale_id`,`print_sequence`,`tax_type`,`tax_group`);

--
-- Indices de la tabla `ospos_sessions`
--
ALTER TABLE `ospos_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indices de la tabla `ospos_stock_locations`
--
ALTER TABLE `ospos_stock_locations`
  ADD PRIMARY KEY (`location_id`);

--
-- Indices de la tabla `ospos_suppliers`
--
ALTER TABLE `ospos_suppliers`
  ADD UNIQUE KEY `account_number` (`account_number`),
  ADD KEY `person_id` (`person_id`);

--
-- Indices de la tabla `ospos_tax_categories`
--
ALTER TABLE `ospos_tax_categories`
  ADD PRIMARY KEY (`tax_category_id`);

--
-- Indices de la tabla `ospos_tax_codes`
--
ALTER TABLE `ospos_tax_codes`
  ADD PRIMARY KEY (`tax_code`);

--
-- Indices de la tabla `ospos_tax_code_rates`
--
ALTER TABLE `ospos_tax_code_rates`
  ADD PRIMARY KEY (`rate_tax_code`,`rate_tax_category_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ospos_customers_packages`
--
ALTER TABLE `ospos_customers_packages`
  MODIFY `package_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `ospos_customers_points`
--
ALTER TABLE `ospos_customers_points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ospos_dinner_tables`
--
ALTER TABLE `ospos_dinner_tables`
  MODIFY `dinner_table_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ospos_giftcards`
--
ALTER TABLE `ospos_giftcards`
  MODIFY `giftcard_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ospos_inventory`
--
ALTER TABLE `ospos_inventory`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT de la tabla `ospos_items`
--
ALTER TABLE `ospos_items`
  MODIFY `item_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `ospos_item_kits`
--
ALTER TABLE `ospos_item_kits`
  MODIFY `item_kit_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ospos_people`
--
ALTER TABLE `ospos_people`
  MODIFY `person_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `ospos_receivings`
--
ALTER TABLE `ospos_receivings`
  MODIFY `receiving_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `ospos_sales`
--
ALTER TABLE `ospos_sales`
  MODIFY `sale_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `ospos_sales_reward_points`
--
ALTER TABLE `ospos_sales_reward_points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ospos_stock_locations`
--
ALTER TABLE `ospos_stock_locations`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ospos_tax_categories`
--
ALTER TABLE `ospos_tax_categories`
  MODIFY `tax_category_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

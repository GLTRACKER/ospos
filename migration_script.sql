-- ----------------------------------------------------------------------------
-- MySQL Workbench Migration
-- Migrated Schemata: osposp
-- Source Schemata: ospos
-- Created: Sun Feb 10 13:22:08 2019
-- Workbench Version: 6.3.10
-- ----------------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------------------------------------------------------
-- Schema osposp
-- ----------------------------------------------------------------------------
DROP SCHEMA IF EXISTS `osposp` ;
CREATE SCHEMA IF NOT EXISTS `osposp` ;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_app_config
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_app_config` (
  `key` VARCHAR(50) NOT NULL,
  `value` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`key`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_customers
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_customers` (
  `person_id` INT(10) NOT NULL,
  `company_name` VARCHAR(255) NULL DEFAULT NULL,
  `account_number` VARCHAR(255) NULL DEFAULT NULL,
  `taxable` INT(1) NOT NULL DEFAULT '1',
  `sales_tax_code` VARCHAR(32) NOT NULL DEFAULT '1',
  `discount_percent` DECIMAL(15,2) NOT NULL DEFAULT '0.00',
  `package_id` INT(11) NULL DEFAULT NULL,
  `points` INT(11) NULL DEFAULT NULL,
  `deleted` INT(1) NOT NULL DEFAULT '0',
  UNIQUE INDEX `account_number` (`account_number` ASC),
  INDEX `person_id` (`person_id` ASC),
  INDEX `package_id` (`package_id` ASC, `deleted` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_customers_packages
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_customers_packages` (
  `package_id` INT(11) NOT NULL AUTO_INCREMENT,
  `package_name` VARCHAR(255) NULL DEFAULT NULL,
  `points_percent` FLOAT NOT NULL DEFAULT '0',
  `deleted` INT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`package_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_customers_points
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_customers_points` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `person_id` INT(11) NOT NULL,
  `package_id` INT(11) NOT NULL,
  `sale_id` INT(11) NOT NULL,
  `points_earned` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `person_id` (`person_id` ASC),
  INDEX `package_id` (`package_id` ASC),
  INDEX `sale_id` (`sale_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_dinner_tables
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_dinner_tables` (
  `dinner_table_id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(30) NOT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '0',
  `deleted` INT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dinner_table_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_employees
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_employees` (
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `person_id` INT(10) NOT NULL,
  `deleted` INT(1) NOT NULL DEFAULT '0',
  `hash_version` INT(1) NOT NULL DEFAULT '2',
  UNIQUE INDEX `username` (`username` ASC),
  INDEX `person_id` (`person_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_giftcards
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_giftcards` (
  `record_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `giftcard_id` INT(11) NOT NULL AUTO_INCREMENT,
  `giftcard_number` VARCHAR(255) NULL DEFAULT NULL,
  `value` DECIMAL(15,2) NOT NULL,
  `deleted` INT(1) NOT NULL DEFAULT '0',
  `person_id` INT(10) NULL DEFAULT NULL,
  PRIMARY KEY (`giftcard_id`),
  UNIQUE INDEX `giftcard_number` (`giftcard_number` ASC),
  INDEX `person_id` (`person_id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_grants
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_grants` (
  `permission_id` VARCHAR(255) NOT NULL,
  `person_id` INT(10) NOT NULL,
  PRIMARY KEY (`permission_id`, `person_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_inventory
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_inventory` (
  `trans_id` INT(11) NOT NULL AUTO_INCREMENT,
  `trans_items` INT(11) NOT NULL DEFAULT '0',
  `trans_user` INT(11) NOT NULL DEFAULT '0',
  `trans_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` TEXT NOT NULL,
  `trans_location` INT(11) NOT NULL,
  `trans_inventory` DECIMAL(15,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`trans_id`),
  INDEX `trans_items` (`trans_items` ASC),
  INDEX `trans_user` (`trans_user` ASC),
  INDEX `trans_location` (`trans_location` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 54
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_item_kit_items
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_item_kit_items` (
  `item_kit_id` INT(11) NOT NULL,
  `item_id` INT(11) NOT NULL,
  `quantity` DECIMAL(15,3) NOT NULL,
  `kit_sequence` INT(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_kit_id`, `item_id`, `quantity`),
  INDEX `ospos_item_kit_items_ibfk_2` (`item_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_item_kits
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_item_kits` (
  `item_kit_id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `item_id` INT(10) NOT NULL DEFAULT '0',
  `kit_discount_percent` DECIMAL(15,2) NOT NULL DEFAULT '0.00',
  `price_option` TINYINT(2) NOT NULL DEFAULT '0',
  `print_option` TINYINT(2) NOT NULL DEFAULT '0',
  `description` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`item_kit_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_item_quantities
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_item_quantities` (
  `item_id` INT(11) NOT NULL,
  `location_id` INT(11) NOT NULL,
  `quantity` DECIMAL(15,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`item_id`, `location_id`),
  INDEX `item_id` (`item_id` ASC),
  INDEX `location_id` (`location_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_items
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_items` (
  `name` VARCHAR(255) NOT NULL,
  `category` VARCHAR(255) NOT NULL,
  `supplier_id` INT(11) NULL DEFAULT NULL,
  `item_number` VARCHAR(255) NULL DEFAULT NULL,
  `description` VARCHAR(255) NOT NULL,
  `cost_price` DECIMAL(15,2) NOT NULL,
  `unit_price` DECIMAL(15,2) NOT NULL,
  `reorder_level` DECIMAL(15,3) NOT NULL DEFAULT '0.000',
  `receiving_quantity` DECIMAL(15,3) NOT NULL DEFAULT '1.000',
  `item_id` INT(10) NOT NULL AUTO_INCREMENT,
  `pic_filename` VARCHAR(255) NULL DEFAULT NULL,
  `allow_alt_description` TINYINT(1) NOT NULL,
  `is_serialized` TINYINT(1) NOT NULL,
  `stock_type` TINYINT(2) NOT NULL DEFAULT '0',
  `item_type` TINYINT(2) NOT NULL DEFAULT '0',
  `tax_category_id` INT(10) NOT NULL DEFAULT '1',
  `deleted` INT(1) NOT NULL DEFAULT '0',
  `custom1` VARCHAR(255) NULL DEFAULT NULL,
  `custom2` VARCHAR(255) NULL DEFAULT NULL,
  `custom3` VARCHAR(255) NULL DEFAULT NULL,
  `custom4` VARCHAR(255) NULL DEFAULT NULL,
  `custom5` VARCHAR(255) NULL DEFAULT NULL,
  `custom6` VARCHAR(255) NULL DEFAULT NULL,
  `custom7` VARCHAR(255) NULL DEFAULT NULL,
  `custom8` VARCHAR(255) NULL DEFAULT NULL,
  `custom9` VARCHAR(255) NULL DEFAULT NULL,
  `custom10` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  UNIQUE INDEX `item_number` (`item_number` ASC),
  INDEX `supplier_id` (`supplier_id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_items_taxes
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_items_taxes` (
  `item_id` INT(10) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `percent` DECIMAL(15,3) NOT NULL,
  PRIMARY KEY (`item_id`, `name`, `percent`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_migrations
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_migrations` (
  `version` BIGINT(20) NOT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_modules
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_modules` (
  `name_lang_key` VARCHAR(255) NOT NULL,
  `desc_lang_key` VARCHAR(255) NOT NULL,
  `sort` INT(10) NOT NULL,
  `module_id` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`module_id`),
  UNIQUE INDEX `desc_lang_key` (`desc_lang_key` ASC),
  UNIQUE INDEX `name_lang_key` (`name_lang_key` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_people
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_people` (
  `item_number` VARCHAR(255) NOT NULL,
  `first_name` VARCHAR(255) NOT NULL,
  `last_name` VARCHAR(255) NOT NULL,
  `gender` INT(1) NULL DEFAULT NULL,
  `phone_number` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `address_1` VARCHAR(255) NOT NULL,
  `address_2` VARCHAR(255) NOT NULL,
  `city` VARCHAR(255) NOT NULL,
  `state` VARCHAR(255) NOT NULL,
  `zip` VARCHAR(255) NOT NULL,
  `country` VARCHAR(255) NOT NULL,
  `comments` TEXT NOT NULL,
  `person_id` INT(10) NOT NULL AUTO_INCREMENT,
  `code` INT(10) NULL DEFAULT NULL,
  PRIMARY KEY (`person_id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC),
  INDEX `email` (`email` ASC),
  INDEX `code` (`code` ASC),
  INDEX `first_name` (`first_name` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 39
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_permissions
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_permissions` (
  `permission_id` VARCHAR(255) NOT NULL,
  `module_id` VARCHAR(255) NOT NULL,
  `location_id` INT(10) NULL DEFAULT NULL,
  PRIMARY KEY (`permission_id`),
  INDEX `module_id` (`module_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_receivings
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_receivings` (
  `receiving_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` INT(10) NULL DEFAULT NULL,
  `employee_id` INT(10) NOT NULL DEFAULT '0',
  `comment` TEXT NULL DEFAULT NULL,
  `receiving_id` INT(10) NOT NULL AUTO_INCREMENT,
  `payment_type` VARCHAR(20) NULL DEFAULT NULL,
  `reference` VARCHAR(32) NULL DEFAULT NULL,
  PRIMARY KEY (`receiving_id`),
  INDEX `supplier_id` (`supplier_id` ASC),
  INDEX `employee_id` (`employee_id` ASC),
  INDEX `reference` (`reference` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_receivings_items
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_receivings_items` (
  `receiving_id` INT(10) NOT NULL DEFAULT '0',
  `item_id` INT(10) NOT NULL DEFAULT '0',
  `description` VARCHAR(30) NULL DEFAULT NULL,
  `serialnumber` VARCHAR(30) NULL DEFAULT NULL,
  `line` INT(3) NOT NULL,
  `quantity_purchased` DECIMAL(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` DECIMAL(15,2) NOT NULL,
  `item_unit_price` DECIMAL(15,2) NOT NULL,
  `discount_percent` DECIMAL(15,2) NOT NULL DEFAULT '0.00',
  `item_location` INT(11) NOT NULL,
  `receiving_quantity` DECIMAL(15,3) NOT NULL DEFAULT '1.000',
  PRIMARY KEY (`receiving_id`, `item_id`, `line`),
  INDEX `item_id` (`item_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_sales
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_sales` (
  `sale_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` INT(10) NULL DEFAULT NULL,
  `employee_id` INT(10) NOT NULL DEFAULT '0',
  `comment` TEXT NULL DEFAULT NULL,
  `invoice_number` VARCHAR(32) NULL DEFAULT NULL,
  `quote_number` VARCHAR(32) NULL DEFAULT NULL,
  `sale_id` INT(10) NOT NULL AUTO_INCREMENT,
  `sale_status` TINYINT(2) NOT NULL DEFAULT '0',
  `dinner_table_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`sale_id`),
  UNIQUE INDEX `invoice_number` (`invoice_number` ASC),
  INDEX `customer_id` (`customer_id` ASC),
  INDEX `employee_id` (`employee_id` ASC),
  INDEX `sale_time` (`sale_time` ASC),
  INDEX `dinner_table_id` (`dinner_table_id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 27
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_sales_items
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_sales_items` (
  `sale_id` INT(10) NOT NULL DEFAULT '0',
  `item_id` INT(10) NOT NULL DEFAULT '0',
  `description` VARCHAR(255) NULL DEFAULT NULL,
  `serialnumber` VARCHAR(30) NULL DEFAULT NULL,
  `line` INT(3) NOT NULL DEFAULT '0',
  `quantity_purchased` DECIMAL(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` DECIMAL(15,2) NOT NULL,
  `item_unit_price` DECIMAL(15,2) NOT NULL,
  `discount_percent` DECIMAL(15,2) NOT NULL DEFAULT '0.00',
  `item_location` INT(11) NOT NULL,
  `print_option` TINYINT(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`, `item_id`, `line`),
  INDEX `sale_id` (`sale_id` ASC),
  INDEX `item_id` (`item_id` ASC),
  INDEX `item_location` (`item_location` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_sales_items_taxes
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_sales_items_taxes` (
  `sale_id` INT(10) NOT NULL,
  `item_id` INT(10) NOT NULL,
  `line` INT(3) NOT NULL DEFAULT '0',
  `name` VARCHAR(255) NOT NULL,
  `percent` DECIMAL(15,4) NOT NULL DEFAULT '0.0000',
  `tax_type` TINYINT(2) NOT NULL DEFAULT '0',
  `rounding_code` TINYINT(2) NOT NULL DEFAULT '0',
  `cascade_tax` TINYINT(2) NOT NULL DEFAULT '0',
  `cascade_sequence` TINYINT(2) NOT NULL DEFAULT '0',
  `item_tax_amount` DECIMAL(15,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`sale_id`, `item_id`, `line`, `name`, `percent`),
  INDEX `sale_id` (`sale_id` ASC),
  INDEX `item_id` (`item_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_sales_payments
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_sales_payments` (
  `sale_id` INT(10) NOT NULL,
  `payment_type` VARCHAR(40) NOT NULL,
  `payment_amount` DECIMAL(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`, `payment_type`),
  INDEX `sale_id` (`sale_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_sales_reward_points
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_sales_reward_points` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `sale_id` INT(11) NOT NULL,
  `earned` FLOAT NOT NULL,
  `used` FLOAT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `sale_id` (`sale_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_sales_taxes
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_sales_taxes` (
  `sale_id` INT(10) NOT NULL,
  `tax_type` SMALLINT(2) NOT NULL,
  `tax_group` VARCHAR(32) NOT NULL,
  `sale_tax_basis` DECIMAL(15,4) NOT NULL,
  `sale_tax_amount` DECIMAL(15,4) NOT NULL,
  `print_sequence` TINYINT(2) NOT NULL DEFAULT '0',
  `name` VARCHAR(255) NOT NULL,
  `tax_rate` DECIMAL(15,4) NOT NULL,
  `sales_tax_code` VARCHAR(32) NOT NULL DEFAULT '',
  `rounding_code` TINYINT(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`, `tax_type`, `tax_group`),
  INDEX `print_sequence` (`sale_id` ASC, `print_sequence` ASC, `tax_type` ASC, `tax_group` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_sessions
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_sessions` (
  `id` VARCHAR(40) NOT NULL,
  `ip_address` VARCHAR(45) NOT NULL,
  `timestamp` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` BLOB NOT NULL,
  INDEX `ci_sessions_timestamp` (`timestamp` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_stock_locations
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_stock_locations` (
  `location_id` INT(11) NOT NULL AUTO_INCREMENT,
  `location_name` VARCHAR(255) NULL DEFAULT NULL,
  `deleted` INT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`location_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_suppliers
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_suppliers` (
  `person_id` INT(10) NOT NULL,
  `company_name` VARCHAR(255) NOT NULL,
  `agency_name` VARCHAR(255) NOT NULL,
  `account_number` VARCHAR(255) NULL DEFAULT NULL,
  `deleted` INT(1) NOT NULL DEFAULT '0',
  UNIQUE INDEX `account_number` (`account_number` ASC),
  INDEX `person_id` (`person_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_tax_categories
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_tax_categories` (
  `tax_category_id` INT(10) NOT NULL AUTO_INCREMENT,
  `tax_category` VARCHAR(32) NOT NULL,
  `tax_group_sequence` TINYINT(2) NOT NULL,
  PRIMARY KEY (`tax_category_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_tax_code_rates
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_tax_code_rates` (
  `rate_tax_code` VARCHAR(32) NOT NULL,
  `rate_tax_category_id` INT(10) NOT NULL,
  `tax_rate` DECIMAL(15,4) NOT NULL DEFAULT '0.0000',
  `rounding_code` TINYINT(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rate_tax_code`, `rate_tax_category_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table osposp.ospos_tax_codes
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `osposp`.`ospos_tax_codes` (
  `tax_code` VARCHAR(32) NOT NULL,
  `tax_code_name` VARCHAR(255) NOT NULL DEFAULT '',
  `tax_code_type` TINYINT(2) NOT NULL DEFAULT '0',
  `city` VARCHAR(255) NOT NULL DEFAULT '',
  `state` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`tax_code`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
SET FOREIGN_KEY_CHECKS = 1;
